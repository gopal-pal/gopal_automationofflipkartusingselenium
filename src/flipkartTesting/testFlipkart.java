package flipkartTesting;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.ArrayList;

public class testFlipkart {
    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "src/chromedriver_linux64/chromedriver");
        WebDriver driver  = new ChromeDriver();
        driver.get("https://flipkart.com/");
        driver.manage().window().maximize();
        driver.findElement(By.xpath("//button[@class='_2KpZ6l _2doB4z']")).click();
        driver.findElement(By.xpath("//input[@class='_3704LK']")).sendKeys("Man's Tshirt");
        driver.findElement(By.xpath("//button[@class='L0Z3Pu']")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("//a[@class='_3bPFwb']")).click();
        Thread.sleep(2000);
        ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(tabs.size() - 1));
        Thread.sleep(2000);
        driver.findElement(By.xpath("//a[@class='_1fGeJ5 _2UVyXR _31hAvz']")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("//button[@class='_2KpZ6l _2U9uOA _3v1-ww']")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("//button[@class='_2KpZ6l _2ObVJD _3AWRsL']")).click();
        Thread.sleep(4000);
        driver.close();
        driver.quit();
    }
}
